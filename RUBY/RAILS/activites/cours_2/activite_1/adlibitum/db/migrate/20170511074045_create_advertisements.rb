class CreateAdvertisements < ActiveRecord::Migration[5.1]
  def change
    create_table :advertisements do |t|
      t.string :title
      t.text :content
      t.string :state
      t.decimal :price
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
