class User < ApplicationRecord
  has_many :comments

  validates :name, presence: {
    notice: 'Name is required'
  }

  validates :password, presence: {
    notice: 'Password is required'
  }

  validates :name, uniqueness: {
    notice: 'Already taken'
  }

  def admin?
    role == 'admin'
  end
end
