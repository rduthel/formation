class Advertisement < ApplicationRecord
  belongs_to :user
  has_many :comments

  validates :title, presence: {
    notice: 'Your ad needs a title'
  }

  validates :title, uniqueness: {
    notice: 'Already posted'
  }

  validates :content, presence: {
    notice: 'A content is required'
  }

  validates :price, presence: {
    notice: 'We cannot guess a price for you'
  }

  validates :price, numericality: true

  scope :published, (-> { where(state: 'published') })
end
