Rails.application.routes.draw do
  root 'advertisements#index'

  get 'advertisements/login' => 'users#login'
  patch 'advertisements/:id' => 'advertisements#publish'
  post 'advertisements/:id' => 'comments#create'
  post 'advertisements/login' => 'users#check'

  get 'users/login' => 'users#login'
  post 'users/login' => 'users#check'
  delete 'users' => 'users#logout'

  resources :comments
  resources :advertisements
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
