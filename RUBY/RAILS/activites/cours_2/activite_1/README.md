### Consignes
Réaliser un site de petites annonces avec modération.

### Détails
- Les internautes peuvent s'inscrire (nom et mdp choisis, rôle attribué automatiquement) ;
- Les utilisateurs peuvent se connecter, et, une fois fait, poster une annonce ;
- Seul-e-s les administrateurs-trices peuvent valider les annonces, ce qui implique que...
- ... Seules les annonces publiées apparaissent à tout internaute et aux inscrit-e-s ;
- Enfin, les inscrit-e-s peuvent commenter les annonces ;
- Et se déconnecter.

Tous les champs affichés sont obligatoires.

*N.B. : l'utilisateur "admin", mdp "admin", a tous les pouvoirs*