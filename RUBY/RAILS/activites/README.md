### Nota Bene
Voici la marche à suivre pour tester ces sites, sur une machine disposant déjà de Ruby :

```
cd <dossier_de_l_activite>
bundle install #installe les dépendances
rails server #lance le serveur
```
