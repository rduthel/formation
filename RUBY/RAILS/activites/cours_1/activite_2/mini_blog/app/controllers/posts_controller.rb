class PostsController < ApplicationController
  def index
    @posts = Post.page(params[:page]).per(5)
  end

  def show
    @post = Post.find(params[:id])
    @comments = Comment.all.reverse.select { |c| c.post_id == @post.id }
  end

  def create
    @comment = Comment
    if @comment.create author: params[:author], content: params[:content], post_id: params[:post_id]
      redirect_to "/posts/#{params[:id]}"
    else
      render :show
    end
  end
end
