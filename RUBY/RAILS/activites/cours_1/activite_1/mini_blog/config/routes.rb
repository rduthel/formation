Rails.application.routes.draw do
  root 'posts#index'
  get 'posts' => 'posts#index'
  get 'posts/:id' => 'posts#show'
  post 'posts/:id' => 'posts#create'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
