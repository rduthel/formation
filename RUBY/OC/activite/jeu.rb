class Personne # :nodoc:
  # le nodoc est juste la pour dire a Ruby que non,
  # on ne veut pas rediger de documentation pour cette classe
  attr_reader :nom
  attr_accessor :points_de_vie, :en_vie

  def initialize(nom)
    @nom = nom
    @points_de_vie = 100
    @en_vie = true
  end

  def info
    # - Renvoie le nom et les points de vie si la personne est en vie
    if @points_de_vie > 0
      "#{nom} (#{@points_de_vie}/100 pv)"
    elsif points_de_vie <= 0
      # - Renvoie le nom et "vaincu" si la personne a ete vaincue
      @en_vie = false
      "#{nom} (vaincu)"
    end
  end

  def attaque(personne)
    # - Affiche ce qu'il se passe
    puts "#{@nom} attaque #{personne.nom}."
    # - Fait subir des degats a la personne passee en parametre
    personne.subit_attaque(degats)
  end

  def subit_attaque(degats_recus)
    # - Reduit les points de vie en fonction des degats recus
    @points_de_vie -= degats_recus
    # si les points de vie sont en negatif, on les ajoute aux degats recus
    # de cette maniere le jeu n'affichera pas quelque chose comme :
    # Squelette (10 PV) subit 30 points de degats.
    degats_recus += @points_de_vie if @points_de_vie < 0
    # - Affiche ce qu'il s'est passe
    #  et determine si la personne est toujours en vie ou non
    puts "#{@nom} subit #{degats_recus} points de dégâts."
    @en_vie = if @points_de_vie > 0
                true
              else
                puts "#{@nom} meurt."
                false
              end
  end
end

class Joueur < Personne # :nodoc:
  attr_reader :degats_bonus

  def initialize(nom)
    # Par defaut le joueur n'a pas de degats bonus
    @degats_bonus = 0

    # Appelle le "initialize" de la classe mere (Personne)
    super(nom)
  end

  def degats
    # - Calcule les degats
    degats = rand 10..30
    degats += @degats_bonus
    # - Affiche ce qu'il s'est passe
    puts "#{nom} bénéficie de #{degats_bonus} point(s) de dégâts en bonus."
    degats
  end

  def soin
    # - Augmente les points de vie
    # en fonction des points de vie restants
    # et de facon a ne pas les depasser
    case @points_de_vie
    when 0..60
      bonus_vie = rand 20..40
    when 60..80
      bonus_vie = rand 10..(100 - @points_de_vie)
    when 80..100
      bonus_vie = rand 0..(100 - @points_de_vie)
    end
    @points_de_vie += bonus_vie
    # - Affiche ce qu'il s'est passe
    puts "#{nom} a récupéré #{bonus_vie} point(s) de vie."
  end

  def ameliorer_degats
    # - Augmente les degats bonus
    @degats_bonus = rand 15..30
    # - Affiche ce qu'il s'est passe
    puts "#{nom} se concentre : son attaque augmente de #{degats_bonus} !"
  end
end

class Ennemi < Personne # :nodoc:
  def degats
    rand 1..7 # soyons gentils avec notre heros-ine ;)
  end
end

class Jeu # :nodoc:
  def self.actions_possibles(monde)
    puts 'ACTIONS POSSIBLES :'

    puts '0 - Se soigner'
    puts '1 - Améliorer son attaque'

    # On commence a 2 car 0 et 1 sont reserves pour les actions
    # de soin et d'amelioration d'attaque
    i = 2
    monde.ennemis.each do |ennemi|
      puts "#{i} - Attaquer #{ennemi.info}"
      i += 1
    end
    puts '99 - Quitter'
  end

  def self.est_fini(joueur, monde)
    # - Determine la condition de fin du jeu :
    # si le joueur n'est plus en vie OU si tous les ennemis sont morts
    # NB: "length.zero?" = "length == 0"
    if !joueur.en_vie || monde.ennemis_en_vie.length.zero?
      true
    else
      false
    end
  end
end

class Monde # :nodoc:
  attr_accessor :ennemis

  def ennemis_en_vie
    # - Ne retourne que les ennemis en vie
    # NB: "ennemis.select(&:en_vie)" =
    #     "ennemis.select { |e| e.en_vie }"
    ennemis.select(&:en_vie)
  end
end

##############

# Initialisation du monde
monde = Monde.new

# Ajout des ennemis
monde.ennemis = [
  Ennemi.new('Balrog'),
  Ennemi.new('Gobelin'),
  Ennemi.new('Squelette')
]

# Initialisation du joueur
# La encore une petite fantaisie de ma part :
# vous donnez le nom que vous voulez au personnage
print 'Entrez le nom de votre héros ou héroïne : '
joueur = Joueur.new(gets.chomp.to_s)

# Message d'introduction. \n signifie "retour a la ligne"
puts "\n\nAinsi débutent les aventures de #{joueur.nom}\n\n"

# Boucle de jeu principale
100.times do |tour|
  puts "\n------------------ Tour numéro #{tour} ------------------"

  # Affiche les differentes actions possibles
  Jeu.actions_possibles(monde)

  puts "\nQUELLE ACTION FAIRE ?"
  # On range dans la variable "choix" ce que l'utilisateur renseigne
  choix = gets.chomp.to_i

  # En fonction du choix on appelle differentes methodes sur le joueur
  # NB: "choix.zero?" = "choix == 0"
  if choix.zero?
    joueur.soin
  elsif choix == 1
    joueur.ameliorer_degats
  elsif choix == 99
    # On quitte la boucle de jeu si on a choisi
    # 99 qui veut dire "quitter"
    break
  elsif choix == 2 || choix == 3 || choix == 4
    # Choix - 2 car nous avons commence a compter a partir de 2
    # les choix 0 et 1 etant reserves pour le soin et
    # l'amelioration d'attaque
    ennemi_a_attaquer = monde.ennemis[choix - 2]
    # si l'ennemi a attaquer est deja mort...
    unless ennemi_a_attaquer.en_vie
      # on relance le tour apres avoir alerte le joueur
      # d'ailleurs le "\a" provoque une alerte sonore normalement ;)
      puts "Déjà vaincu, veuillez ré-essayer\a"
      redo
    end
    joueur.attaque(ennemi_a_attaquer)
    # sinon, si erreur de saisie, on relance le tour
  else
    puts "Erreur de saisie, veuillez ré-essayer\a"
    redo
  end

  # J'ai pris le parti d'ajouter cette condition pour eviter que
  # "LES ENNEMIS RIPOSTENT" s'affiche alors qu'ils etaient morts ;)
  # NB: "unless monde.ennemis_en_vie.empty?" =
  #     "if monde.ennemis_en_vie.length != 0"
  unless monde.ennemis_en_vie.empty?
    puts "\nLES ENNEMIS RIPOSTENT !"
    # Pour tous les ennemis en vie...
    monde.ennemis_en_vie.each do |ennemi|
      # ...le hero subit une attaque.
      ennemi.attaque(joueur)
      # encore une petite condition pour eviter
      # les morts multiples du personnage dans un meme tour
      break if Jeu.est_fini(joueur, monde)
    end
  end

  # Si le jeu est fini, on interompt la boucle
  break if Jeu.est_fini(joueur, monde)

  puts "\nÉtat du perso : #{joueur.info}\n"
end

puts "\nGame Over!\n"

# si le jeu est fini (donc que vous n'avez pas choisi de quitter la partie)
# et le joueur encore en vie :
# NB: "\r" correspond a un retour en debut de ligne
if Jeu.est_fini(joueur, monde) && joueur.en_vie
  puts "Tous les ennemis ont été vaincus mais #{joueur.nom} est encore vivant-e,
  \ravec #{joueur.points_de_vie} PV restants.
  \rVous avez donc remporté cette partie !"

# sinon si le jeu est termine mais le joueur mort :
elsif Jeu.est_fini(joueur, monde) && !joueur.en_vie
  puts "Malheureusement #{joueur.nom} a été défait-e...
  \r#{monde.ennemis_en_vie.length} ennemi(s) est/sont encore en vie."
end
