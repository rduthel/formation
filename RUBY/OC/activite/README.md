### Consignes
Création d'un jeu de rôle tout par tour mode texte en POO.

Celui-ci met en scène un personnage, Jean-Michel Paladin dans l'énoncé, qui doit défaire trois ennemis, soit un balrog, un squelette et un gobelin. Sieur Paladin dispose de plusieurs actions par tour :
- soin
- attaque
- amélioration de l'attaque.


### Ajouts personnels
J'ai donné la liberté au - à la joueur - joueuse de nommer son personnage.

Des conditions relatives aux points de vie des différents protagonistes ont été ajoutées (ceux du héros ne peuvent dépasser les 100, et celui-ci ne peut attaquer un ennemi déjà mort).

Les erreurs de frappe sont également gérées, et un son est joué le cas échéant.
