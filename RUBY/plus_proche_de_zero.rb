def plus_proche_de_zero(nombres)
  nombres.min_by(&:abs)
end

nombres = [1, 2, -4, -0.1, -5]

puts "Le nombre le plus proche de 0 est #{plus_proche_de_zero(nombres)}."
