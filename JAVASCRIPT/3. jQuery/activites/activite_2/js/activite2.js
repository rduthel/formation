$(function() {
	// fonction de génération du formulaire à droite, et de l'insertion des champs à gauche, suivant le texte du label et le type de champ
	function genererFormulaire(texte, typeChamp) {

		// une fonction de suppression du formulaire pour éviter les redites
		function suppressionProgressive(element, vitesse) {
			// si on n'annule pas le comportement par défaut, la page se recharge sans lancer l'animation...
			element.preventDefault();
			// disparition du forumulaire, en s'auto-supprimant, et en supprimant l'attribut « disabled » des boutons
			formulaireConteneur.fadeOut(vitesse, function() {
				$(this).remove();
				$("button").removeAttr("disabled");
			});
		}

		// désactivation des boutons du haut
		$("button").attr("disabled", "true");
		// insertion à droite après la ligne horizontale d'un formulaire avec texte, zone de saisie, et bouton de validation
		$("hr").after("<form id=\"formulaireConteneur\">" + texte + "<input type=\"text\" id=\"texteInput\"><button id=\"boutonOK\">OK</button><button id=\"boutonAnnulation\">Annuler</button></form>");
		$("#formulaireConteneur").hide().fadeIn();

		// on donne le focus au champ de saisie
		$("#texteInput").focus();

		var formulaireConteneur = $("#formulaireConteneur");

		// au clic sur le bouton de validation
		$("#boutonOK").click(function(e) {
			// création d'une variable nulle
			var objet;
			// récupération de la valeur entrée par l'utilisateur-trice dans la zone de saisie
			var texteValue = $("#texteInput").val();

			// suivant le typeChamp, l'objet retourné changera de balise
			switch (typeChamp) {
				case "span":
				objet = "<span>" + texteValue + "</span>";
				break;
				case "input":
				objet = "<input type=\"text\" id =" + texteValue + "\"><br>";
				break;
				case "button":
				objet = "<button>" + texteValue + "</button> <br>";
				break;
				default:
				console.error("Erreur dans la fonction « genererFormulaire ».");
			}
			// ajout de la variable « objet » dans la divGauche
			$("#gauche").append(objet);
			// suppression du formulaireConteneur et réactivation des boutons
			suppressionProgressive(e, 200);
		});

		// au clic sur le bouton « Annuler », le formulaire disparaît
		$("#boutonAnnulation").click(function(e) {
			suppressionProgressive(e, "fast");
		})
	};

	// appel de la fonction genererFormulaire au clic sur les différents boutons
	$("#boutonLabel").click(function() {
		genererFormulaire("Texte du label", "span");
	});

	$("#boutonTexte").click(function() {
		genererFormulaire("id de la zone de texte", "input");
	});

	$("#boutonBouton").click(function() {
		genererFormulaire("Texte du bouton", "button");
	});
});
