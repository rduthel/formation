$(function() {
	// récupération des <div> gauche et droite et de la ligne horizontale dans des variables (cf. optimisation...)
	var divGauche = $("#gauche");
	var divDroite = $("#droite");
	var hrElt = $("hr");

	// fonction de génération du formulaire à droite, et de l'insertion des champs à gauche, suivant le texte du label et le type de champ
	function genererFormulaire(texte, typeChamp) {
		// insertion en divDroite après la ligne horizontale d'un formulaire avec texte, zone de saisie, et bouton de validation
		hrElt.after("<form id=\"formulaireConteneur\">" + texte + "<input type=\"text\" id=\"texteInput\"><button id=\"boutonOK\">OK</button></form>");

		// récupération des éléments précédemment créés dans des variables
		var formulaireConteneur = $("#formulaireConteneur");
		var boutonOK = $("#boutonOK");

		// au clic sur le bouton de validation
		$(boutonOK).click(function() {
			// création d'une variable nulle
			var objet;
			// récupération de la valeur entrée par l'utilisateur-trice dans la zone de saisie
			var texteValue = $("#texteInput").val();
			// suivant le typeChamp, l'objet retourné changera de balise
			switch (typeChamp) {
				case "span":
					objet = "<span>" + texteValue + "</span>";
					break;
				case "input":
					objet = "<input type=\"text\" id =" + texteValue + "\"><br>";
					break;
				case "button":
					objet = "<button>" + texteValue + "</button>";
					break;
				default:
				console.error("Erreur dans la fonction « genererFormulaire ».");
			}
			// ajout de la balise dans la divGauche
			divGauche.append(objet);
			// suppression du formulaireConteneur
			formulaireConteneur.remove();
		});
	};

	// appel de la fonction genererFormulaire au clic sur les différents boutons
	var boutonLabel = $("#boutonLabel");
	$(boutonLabel).click(function() {
		genererFormulaire("Texte du label", "span");
	});

	var boutonTexte = $("#boutonTexte");
	$(boutonTexte).click(function() {
		genererFormulaire("id de la zone de texte", "input");
	});

	var boutonBouton = $("#boutonBouton");
	$(boutonBouton).click(function() {
		genererFormulaire("Texte du bouton", "button");
	});
});
