$(function() {
	// récupération des deux div dans des variables
	var divGauche = $("#gauche");
	var divDroite = $("#droite");


	// fonction de génération du formulaire à droite, et de l'insertion des champs à gauche, suivant le texte du label et le type de champ
	function genererFormulaire(texteLabel, typeChamp) {
		// création d'une ligne de séparation, d'un formulaire et d'un champ « label », dont le texte est celui de texteLabel
		var ligneHorizontale = "<br><br><hr><br>";
		var formElt = document.createElement("form");
		var labelElt = document.createElement("label");
		labelElt.textContent = texteLabel;
		$(labelElt).css("margin-right", "1em");

		// création d'un champ de saisie
		var inputElt = document.createElement("input");
		inputElt.type = "text";
		inputElt.name = "label";
		$(inputElt).css("width", "5em").css("margin-right", "1em");

		// création d'un bouton de validation
		var submitElt = document.createElement("input");
		submitElt.type = "submit";
		submitElt.value = " OK";

		// tout ce qui précède est ajouté au formulaire du divDroite
		$(formElt).append(ligneHorizontale).append(labelElt).append(inputElt).append(submitElt);

		// à l'appui sur le bouton de validation (« OK » dans le divDroite)
		formElt.addEventListener("submit", function(e) {
			// annuler le rechargement de la page
			e.preventDefault();
			// si le typeChamp est "input"
			if (typeChamp === "input") {
				// ajouter dans le divGauche une balise input de type textuel et dont la value est celle que l'utilisateur-trice a entré dans l'inputElt de la divDroite
				var inputGaucheElt = "<input id=\"" + inputElt.value + "\" type=\"text\">";
				$(inputGaucheElt).css("margin", "1em");
				divGauche.append(inputGaucheElt).append("<br><br>");
			}
			// sinon si le typeChamp est de type span ou button
			else if (typeChamp === "span" || typeChamp === "button") {
				// ajouter dans la divGauche un élément de type typeChamp, dont le contenu textuel reprend ce que l'utilisateur-trice a entré
				var eltGauche = document.createElement(typeChamp);
				eltGauche.textContent = inputElt.value;
				$(eltGauche).css("margin", "1em");
				divGauche.append(eltGauche);
			}
			// si rien de tout ça, afficher une erreur en console
			else {
				console.error("Erreur de traitement.");
			}
			// et de toute façon, supprimer le contenu du formulaire
			formElt.innerHTML = "";
		})

		// ajouter le formulaire de saisie dans la divDroite
		divDroite.append(formElt);
	}

	// et au clic sur les premier, deuxième et troisième bouton, appel de la fonction genererFormulaire
	var boutonLabel = $("button")[0];
	$(boutonLabel).click(function() {
		genererFormulaire("Texte du label", "span");
	})

	var boutonTexte = $("button")[1];
	$(boutonTexte).click(function() {
		genererFormulaire("id de la zone de texte", "input");
	})

	var boutonBouton = $("button")[2];
	$(boutonBouton).click(function() {
		genererFormulaire("Texte du bouton", "button");
	})
})
