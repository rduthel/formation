/*
Activité 1
*/

// Liste des liens Web à afficher. Un lien est défini par :
// - son titre
// - son URL
// - son auteur (la personne qui l'a publié)
var listeLiens = [
	{
		titre: "So Foot",
		url: "http://sofoot.com",
		auteur: "yann.usaille"
	},
	{
		titre: "Guide d'autodéfense numérique",
		url: "http://guide.boum.org",
		auteur: "paulochon"
	},
	{
		titre: "L'encyclopédie en ligne Wikipedia",
		url: "http://Wikipedia.org",
		auteur: "annie.zette"
	}
];

// Crée et renvoie un élément DOM affichant les données d'un lien
// Le paramètre lien est un objet JS représentant un lien
function creerElementLien(lien) {
	var titreLien = document.createElement("a");
	titreLien.href = lien.url;
	titreLien.style.color = "#428bca";
	titreLien.style.textDecoration = "none";
	titreLien.style.marginRight = "5px";
	titreLien.appendChild(document.createTextNode(lien.titre));

	var urlLien = document.createElement("span");
	urlLien.appendChild(document.createTextNode(lien.url));

	// Cette ligne contient le titre et l'URL du lien
	var ligneTitre = document.createElement("h4");
	ligneTitre.style.margin = "0px";
	ligneTitre.appendChild(titreLien);
	ligneTitre.appendChild(urlLien);

	// Cette ligne contient l'auteur
	var ligneDetails = document.createElement("span");
	ligneDetails.appendChild(document.createTextNode("Ajouté par " + lien.auteur));

	var divLien = document.createElement("div");
	divLien.classList.add("lien");
	divLien.appendChild(ligneTitre);
	divLien.appendChild(ligneDetails);

	return divLien;
}

var contenu = document.getElementById("contenu");

// Parcours de la liste des liens et ajout d'un élément au DOM pour chaque lien
listeLiens.forEach(function (lien) {
	var elementLien = creerElementLien(lien);
	contenu.appendChild(elementLien);
});


//	______________________________
// |															|
// |			 AJOUTS PERSONNELS			|
// |______________________________|

// fonction de création d'élément « input », vu qu'on en fait plein après...
function creerInputElt(id, type, texteDeRemplacement, largeur) {
	var inputElt = document.createElement("input");
	inputElt.id = id;
	inputElt.type = type;
	inputElt.placeholder = texteDeRemplacement;
	inputElt.required = true;
	inputElt.style.marginBottom = margeInferieure;
	inputElt.style.marginRight = margeDroite;
	inputElt.style.width = largeur;
	return inputElt;
}

// définitions des marges
var margeInferieure = "20px";
var margeDroite = "8px";

// bouton d'ajout de lien
var boutonAjoutLien = document.createElement("button");
boutonAjoutLien.textContent = "Ajouter un lien";
boutonAjoutLien.style.marginBottom = margeInferieure;
contenu.insertBefore(boutonAjoutLien, contenu.firstChild);


// au clic sur ce bouton...
boutonAjoutLien.addEventListener("click", function() {
	// création d'un formulaire avec 3 champs de texte et un bouton de soumission
	var formElt = document.createElement("form");
	var auteurElt = creerInputElt("auteur", "text", "Votre nom", "150px");
	var titreElt = creerInputElt("titre", "text", "Titre du lien", "350px");
	var adresseElt = creerInputElt("adresse", "text", "Adresse du lien", "350px");
	var validationElt = creerInputElt("validation", "submit", "Valider", "");
	formElt.appendChild(auteurElt);
	formElt.appendChild(titreElt);
	formElt.appendChild(adresseElt);
	formElt.appendChild(validationElt);
	// le tout ajouté à la place du boutonAjoutLien
	contenu.replaceChild(formElt, boutonAjoutLien);


	// lors du clic sur le bouton de soumission du formulaire...
	formElt.addEventListener("submit", function(e) {
		// désactivation du comportement de soumision par défaut
		e.preventDefault();
		// test de la présence des caractères « http:// » ou « https:// » en début d'adresse du lien ajouté
		var regex = /https*:\/\/./;
		if (!regex.test(adresseElt.value)) {
			adresseElt.value = "http://"+adresseElt.value;
		}

		// création du lienFinal
		var lienFinal = creerElementLien({titre: formElt.titre.value, url: formElt.adresse.value, auteur: formElt.auteur.value});
		// inséré avant le premier lien de la page
		contenu.insertBefore(lienFinal, document.getElementsByClassName("lien")[0]);

		// remplacement du formulaire par le boutonAjoutLien
		contenu.replaceChild(boutonAjoutLien, formElt);

		// confirmation d'ajout
		var confirmationElt = document.createElement("h4");
		confirmationElt.textContent = "Le lien \"" + titreElt.value + "\" a bien été ajouté.";
		confirmationElt.style.margin = margeInferieure;
		confirmationElt.style.color = "#428bca";
		contenu.insertBefore(confirmationElt, boutonAjoutLien);

		// suppression du message de confirmation au bout de 2 secondes
		setTimeout(function() {
			contenu.removeChild(confirmationElt);
		}, 2000)
	})
})
